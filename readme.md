
Application Installation

1. Clone the project with ```git clone https://bitbucket.org/esgigroupe1/equaton.git```

2. Run ```python3 -m venv venv``` to create virtual environment

3. Run ```source venv/bin/activate``` to activate virtual environment

4. Run ```pip install django``` to install django

5 . Check that django is really installed by running :
```
   $ python
   >>> import django
   >>> exit()
```

6. Run ```pip install django-widget-tweaks``` to install form customizer

7. Navigate through cloned project here 'equaton' #Linux ``` cd equaton``` #Windows ```pushd equaton```

8. Run ```python manage.py migrate``` to run all migrations

9. Run  ```python manage.py runserver``` to start server #Enjoy