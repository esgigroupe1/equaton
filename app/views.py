from django.shortcuts import render
from .forms import EquationForm
from math import sqrt
from django.http import HttpResponseRedirect

from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

# Create your views here.

def home(request):
    # POST
    if request.method == 'POST':

    # create a form instance and populate it with data from the request:
        form = EquationForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            #get equation coefficients and check if there are int
            try:
                a = int(form['a'].value())
                b = int(form['b'].value())
                c = int(form['c'].value())
            except:
                return render(request, 'pages/error_form.html')

            if not c:
                c = 0
            deg = int(form['deg_equation'].value())

            # traiter les équations
            if deg == 2:
                equation,result = equation2D(a,b,c)
            elif deg == 1:
                equation,result = equation1D(a,b)
            else:
                result = "Veuillez re-sélectioner le degré de l'équation que vous souhaitez résoudre."

            #return HttpResponseRedirect('/thanks/')
            return render(request, 'pages/validation.html', {'result': result,'equation': equation})
        else:
            print("formulaire non valide en post")

    # GET
    else:
        form = EquationForm()
        print("formulaire en get")
        return render(request, 'pages/home.html', {'form': form})

def equation2D(a,b,c):
    #display equation
    if b<0 and c<0:
        equation = "%sx2 %sx %s = 0" %(a,b,c)
    elif b<0 and c>0:
        equation = "%sx2 %sx + %s = 0" %(a,b,c)
    elif b>0 and c<0:
        equation = "%sx2 + %sx %s = 0" %(a,b,c)
    else:
        equation = "%sx2 + %sx + %s = 0" %(a,b,c)

    #resolve equation
    delta = b**2-4*a*c
    if delta < 0:
        result = "L'équation n'admet pas de racines"
    elif delta == 0:
        racine = (-b/2*a)
        result = "L'équation admet une racine : %s " % round(racine,2)
    else:
        racine1 = ((-b-sqrt(delta))/(2*a))
        racine2 = ((-b+sqrt(delta))/(2*a))
        result = "L'équation admet deux racines :  %s et %s " % (round(racine1,2), round(racine2,2))
    return (equation,result)


def equation1D(a,b):
    #display equation
    if b<0:
        equation = "%sx %s = 0" %(a,b)
    else:
        equation = "%sx + %s = 0" %(a,b)

    #resolve equation
    if a!=0:
        result ="La solution de l'équation : %s est %s" %(equation,-b/a)
    elif b==0:
        result = "Tous les nombres de l'ensemble IR sont des solutions de l'équation : %s = 0" %(equation)
    else:
        result = "Aucune solution dans IR pour l'équation : %s = 0" %(equation)
    return (equation,result)

# class for registration
class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'