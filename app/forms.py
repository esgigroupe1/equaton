from django import forms

CHOICES = (('1', ' premier dégré',), ('2', ' second degré',))

class EquationForm(forms.Form):

    deg_equation = forms.ChoiceField(widget=forms.Select(attrs={"class":"form-control"}), choices=CHOICES, label="Équation du ",required = True)
    a = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}), max_length=100,required = True)
    b = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}), max_length=100,required = True)
    c = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}), max_length=100,required = False)
